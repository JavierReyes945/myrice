# My Rice

Personal computers rice config and dot files for Windows 10 combined with WSL 1
and 2.

## Getting Started

THis is a WIP repository.

### Prerequisites

* Windows 10
* WSL 1 (and/or 2)
* Ubuntu 20.04 LTS

### Programs

* Bash
* Vim
* Tmux
* VSCode
* Powershell
* Rofi
* Dunst
* Ranger
* nnn
* [i3-gaps](https://github.com/Airblader/i3)
* [Polybar](https://github.com/jaagr/polybar)

## Authors

* **Javier Reyes** - [https://github.com/JavierReyes945/](https://github.com/JavierReyes945/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
