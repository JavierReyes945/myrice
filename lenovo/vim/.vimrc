scriptencoding utf-8
set encoding=utf-8

set number
set laststatus=2
"set colorcolumn=80,120
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
"set listchars=space:Ã‚Â·,tab:Ã‚Â»
set backupdir=~/.vim/tmp//
set directory=~/.vim/tmp//
set undodir=~/.vim/tmp//

" Start of Vundle config
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'morhetz/gruvbox'
call vundle#end()

filetype plugin indent on
" End of Vundle config

set termguicolors
let g:gruvbox_italic=1
set background=dark
colorscheme gruvbox