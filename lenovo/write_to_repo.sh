#!/bin/bash

# Config script to save the config files for the NUC Skull Canyon
# Author: Javier Reyes
# Last modified: 7.5.2018

WSL_SRC=/home/javier
DEST=/home/javier/repos/myrice/nuc
WIN_SRC=/mnt/c/Users/Javier

################################################################################
# CLI Programs
################################################################################

# bash
if [ -f "$WSL_SRC/.bashrc" ]; then
    cp $WSL_SRC/.bashrc $DEST/bash
fi

# vim
if [ -f "$WSL_SRC/.vimrc" ]; then
    cp $WSL_SRC/.vimrc $DEST/vim
fi

# tmux
if [ -f "$WSL_SRC/.tmux.conf" ]; then
    cp $WSL_SRC/.tmux.conf $DEST/tmux
fi

# x
if [ -f "$WSL_SRC/.Xresources" ]; then
    cp $WSL_SRC/.Xresources $DEST/x
fi

# powershell
if [ -f "$WIN_SRC/Documents/WindowsPowerShell/profile.ps1" ]; then
    cp $WIN_SRC/Documents/WindowsPowerShell/profile.ps1 $DEST/powershell
fi

# vscode
if [ -f "$WIN_SRC/AppData/Roaming/Code/User/settings.json" ]; then
    cp $WIN_SRC/AppData/Roaming/Code/User/settings.json $DEST/vscode
fi

# winterm
if [ -f "$WIN_SRC/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json" ]; then
    cp $WIN_SRC/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json $DEST/winterm
fi

################################################################################
# GUI Programs
################################################################################

# i3
if [ -f "$WSL_SRC/.config/i3/config" ]; then
    cp $WSL_SRC/.config/i3/config $DEST/i3
fi

# polybar
if [ -f "$WSL_SRC/.config/polybar/launch.sh" ]; then
    cp $WSL_SRC/.config/polybar/launch.sh $DEST/polybar
fi
if [ -f "$WSL_SRC/.config/polybar/config" ]; then
    cp $WSL_SRC/.config/polybar/config $DEST/polybar
fi

# rofi
if [ -f "$WSL_SRC/.config/rofi/config" ]; then
    cp $WSL_SRC/.config/rofi/config $DEST/rofi
fi

# zathura
if [ -f "$WSL_SRC/.config/zathura/zathurarc" ]; then
    cp $WSL_SRC/.config/zathura/zathurarc $DEST/zathura
fi

# dunst
if [ -f "$WSL_SRC/.config/dunst/dunstrc" ]; then
    cp $WSL_SRC/.config/dunst/dunstrc $DEST/dunst
fi
