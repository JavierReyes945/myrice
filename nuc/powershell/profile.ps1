# Shows navigable menu of all options when hitting Tab
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Autocompletion for arrow keys
Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

# Default location
Set-Location C:\Users\Javier\devel

# Fix dark colors
Set-PSReadLineOption -Colors @{
    "Operator" = [ConsoleColor]::Magenta;
    "Parameter" = [ConsoleColor]::Cyan;
    "Type"      = [ConsoleColor]::Blue;
}

function prompt {
    if ($?) {
        $last_cmd = "Green"
    } else {
        $last_cmd = "Red"
    }
    Write-Host -NoNewLine "[PS] $(Get-Location)`n"
    Write-Host -NoNewLine "$" -foregroundColor $last_cmd
    Return " "
}
