#!/bin/bash

# Config script to save the config files for the NUC Skull Canyon
# Author: Javier Reyes
# Last modified: 7.5.2018

SRC=/home/javier/repos/myrice/nuc
WSL_DEST=/home/javier
WIN_DEST=/mnt/c/Users/Javier

################################################################################
# CLI Programs
################################################################################

# bash
cp $SRC/bash/.bashrc $WSL_DEST

# vim
cp $SRC/vim/.vimrc $WSL_DEST
git clone https://github.com/VundleVim/Vundle.vim.git $WSL_DEST/.vim/bundle/Vundle.vim

# tmux
cp $SRC/tmux/.tmux.conf $WSL_DEST
git clone https://github.com/tmux-plugins/tpm $WSL_DEST/.tmux/plugins/tpm

# x
cp $SRC/x/.Xresources $WSL_DEST

# powershell
if [ ! -f "$WIN_DEST/Documents/WindowsPowerShell/profile.ps1" ]; then
    mkdir $WIN_DEST/Documents/WindowsPowerShell
fi
cp $SRC/powershell/profile.ps1 $WIN_DEST/Documents/WindowsPowerShell

# vscode
if [ ! -f "$WIN_DEST/AppData/Roaming/Code/User/settings.json" ]; then
    mkdir $WIN_DEST/AppData/Roaming/Code/User
fi
cp $SRC/vscode/settings.json $WIN_DEST/AppData/Roaming/Code/User

################################################################################
# GUI Programs
################################################################################
if [ "$1" == "-g" ]; then
    # i3
    cp $SRC/i3/config $DEST/.config/i3

    # polybar
    cp $SRC/polybar/config $SRC/polybar/launch.sh $DEST/.config/polybar

    # rofi
    cp $SRC/rofi/config $DEST/.config/rofi

    # zathura
    cp $SRC/zathura/zathurarc $DEST/.config/zathura

    # dunst
    cp $SRC/dunst/dunstrc $DEST/.config/dunst
fi
